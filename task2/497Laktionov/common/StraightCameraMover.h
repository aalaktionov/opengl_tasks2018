#ifndef STUDENTTASKS2017_STRAIGHTCAMERAMOVER_H
#define STUDENTTASKS2017_STRAIGHTCAMERAMOVER_H


#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>
#include "Camera.hpp"
#include "../Maze.h"

class StraightCameraMover : public CameraMover
{
public:
    StraightCameraMover(MazePtr);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;
    bool isBlocked();
    bool isWall(float y, float x);

protected:
    glm::vec3 _pos;
    glm::quat _rot;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;

    MazePtr _maze;
};


#endif //STUDENTTASKS2017_STRAIGHTCAMERAMOVER_H
