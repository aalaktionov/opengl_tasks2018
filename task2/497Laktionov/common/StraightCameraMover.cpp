#include "StraightCameraMover.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

StraightCameraMover::StraightCameraMover(MazePtr maze_ptr) :
        CameraMover(),
        _pos(0.5f, 1.5f, 0.15f),
        _maze(maze_ptr)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 1.0f, 0.15f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void StraightCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}


void StraightCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void StraightCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}


bool StraightCameraMover::isBlocked() {
    float threshold = 0.2;
    
    return isWall(_pos.y + threshold, _pos.x + threshold)
        || isWall(_pos.y + threshold, _pos.x - threshold)
        || isWall(_pos.y - threshold, _pos.x + threshold)
        || isWall(_pos.y - threshold, _pos.x - threshold);
}

bool StraightCameraMover::isWall(float y, float x) {
    if (x < 0 || y < 0)
        return true;

    int n = _maze->maze_map.size();
    int m = _maze->maze_map[0].size();

    int y_round = int(y);
    int x_round = int(x);
    auto val = y_round >= n || y_round < 0
        || x_round >= m || x_round < 0
        || _maze->maze_map[y_round][x_round] == 'w';
    return val;
}

void StraightCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    auto old_pos = _pos;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    if (isBlocked()) {
        _pos = old_pos;
    }
    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}