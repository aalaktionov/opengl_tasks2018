/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;
uniform int is_light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec3 tanCamSpace;
in vec3 bitanCamSpace;


out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	vec3 tnormal = normalize(texture(normTex, texCoord).rgb * 2 - 1);
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 tan = normalize(tanCamSpace);
	vec3 bitan = normalize(bitanCamSpace);
	normal = -tan * tnormal.x - bitan * tnormal.y + normal * tnormal.z;

	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	float spotLight = 1.0;
	float radius = 0.95;
	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (is_light == 1 && NdotL > 0.0)
	{
		float angle = max(0.0, dot(vec3(0, 0, 1), viewDirection));
		if (angle > radius) {
		    spotLight = (angle - radius) / (1 - radius);
		    spotLight *= min(1, 0.3 / abs(posCamSpace.z));
	    } else {
	        spotLight = 0.0;
	    }
	    spotLight = pow(spotLight, 1) * 0.95 + 0.05;
	    color = diffuseColor * spotLight;
	}

	fragColor = vec4(color, 1.0);
}