#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <fstream>
#include <set>
#include <numeric>
#include <algorithm>
#include <vector>


class MyApplication : public Application {
public:
    std::vector<std::vector<std::vector<char>>> _maze;
    std::vector<MeshPtr> _floors, _ceiling, _walls;
    ShaderProgramPtr _shader;
    TexturePtr _wallTexture, _floorTexture, _ceilingTexture;
    TexturePtr _wallTextureNorm, _floorTextureNorm, _ceilingTextureNorm;
    GLuint _sampler, _normSampler;

    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    MyApplication(std::vector<std::vector<std::vector<char>>> maze) {
        _maze = maze;
    }


    MeshPtr makeFloor(float size = 0.5)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> normals(6, glm::vec3(0.0, 0.0, 1.0));
        std::vector<glm::vec3> tans(6, glm::vec3(1.0, 0.0, 0.0));
        std::vector<glm::vec3> bitans(6, glm::vec3(0.0, 1.0, 0.0));

        //top 1
        vertices.push_back(glm::vec3(-size, size, size));
        vertices.push_back(glm::vec3(size, -size, size));
        vertices.push_back(glm::vec3(size, size, size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));

        //top 2
        vertices.push_back(glm::vec3(-size, size, size));
        vertices.push_back(glm::vec3(-size, -size, size));
        vertices.push_back(glm::vec3(size, -size, size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(tans.size() * sizeof(float) * 3, tans.data());
        DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf4->setData(bitans.size() * sizeof(float) * 3, bitans.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
        mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

    MeshPtr makeCeiling(float size = 0.5)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> normals(6, glm::vec3(0.0, 0.0, -1.0));
        std::vector<glm::vec3> tans(6, glm::vec3(1.0, 0.0, 0.0));
        std::vector<glm::vec3> bitans(6, glm::vec3(0.0, 1.0, 0.0));

        //bottom 1
        vertices.push_back(glm::vec3(-size, size, -size));
        vertices.push_back(glm::vec3(size, size, -size));
        vertices.push_back(glm::vec3(size, -size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //bottom 2
        vertices.push_back(glm::vec3(-size, size, -size));
        vertices.push_back(glm::vec3(size, -size, -size));
        vertices.push_back(glm::vec3(-size, -size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(tans.size() * sizeof(float) * 3, tans.data());
        DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf4->setData(bitans.size() * sizeof(float) * 3, bitans.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
        mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

    MeshPtr makeWalls(float size = 0.5)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec3> tans;
        std::vector<glm::vec3> bitans;

        //front 1
        vertices.push_back(glm::vec3(size, -size, size));
        vertices.push_back(glm::vec3(size, size, -size));
        vertices.push_back(glm::vec3(size, size, size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));

        //front 2
        vertices.push_back(glm::vec3(size, -size, size));
        vertices.push_back(glm::vec3(size, -size, -size));
        vertices.push_back(glm::vec3(size, size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        for (int i = 0; i < 6; i++) {
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
            tans.push_back(glm::vec3(0.0, 1.0, 0.0));
            bitans.push_back(glm::vec3(0.0, 0.0, 1.0));
        }

        //left 1
        vertices.push_back(glm::vec3(-size, -size, size));
        vertices.push_back(glm::vec3(size, -size, -size));
        vertices.push_back(glm::vec3(size, -size, size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));

        //left 2
        vertices.push_back(glm::vec3(-size, -size, size));
        vertices.push_back(glm::vec3(-size, -size, -size));
        vertices.push_back(glm::vec3(size, -size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        for (int i = 0; i < 6; i++) {
            normals.push_back(glm::vec3(0.0, -1.0, 0.0));
            tans.push_back(glm::vec3(1.0, 0.0, 0.0));
            bitans.push_back(glm::vec3(0.0, 0.0, 1.0));
        }

        //back 1
        vertices.push_back(glm::vec3(-size, -size, size));
        vertices.push_back(glm::vec3(-size, size, size));
        vertices.push_back(glm::vec3(-size, size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //back 2
        vertices.push_back(glm::vec3(-size, -size, size));
        vertices.push_back(glm::vec3(-size, size, -size));
        vertices.push_back(glm::vec3(-size, -size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));

        for (int i = 0; i < 6; i++) {
            normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
            tans.push_back(glm::vec3(0.0, 1.0, 0.0));
            bitans.push_back(glm::vec3(0.0, 0.0, 1.0));
        }

        //right 1
        vertices.push_back(glm::vec3(-size, size, size));
        vertices.push_back(glm::vec3(size, size, size));
        vertices.push_back(glm::vec3(size, size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //right 2
        vertices.push_back(glm::vec3(-size, size, size));
        vertices.push_back(glm::vec3(+size, size, -size));
        vertices.push_back(glm::vec3(-size, size, -size));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));

        for (int i = 0; i < 6; i++) {
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
            tans.push_back(glm::vec3(1.0, 0.0, 0.0));
            bitans.push_back(glm::vec3(0.0, 0.0, 1.0));
        }

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(tans.size() * sizeof(float) * 3, tans.data());
        DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf4->setData(bitans.size() * sizeof(float) * 3, bitans.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
        mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }


    void makeScene() override {

        Application::makeScene();


        for (int i = 0; i < _maze.size(); ++i)
            for (int j = 0; j < _maze[i].size(); ++j)
                for (int k = 0; k < _maze[i][j].size(); ++k) {
                    if (_maze[i][j][k] == 1) {
                        _walls.push_back(makeWalls());
                        _walls.back()->setModelMatrix(glm::translate(glm::mat4(1), glm::vec3(j, k, i)));

                    }
                    _ceiling.push_back(makeCeiling());
                    _floors.push_back(makeFloor());
                    _ceiling.back()->setModelMatrix(glm::translate(glm::mat4(1), glm::vec3(j, k, i)));
                    _floors.back()->setModelMatrix(glm::translate(glm::mat4(1), glm::vec3(j, k, i)));
                }
        _cameraMover = std::make_shared<MazeCameraMover>(_maze);
        _shader = std::make_shared<ShaderProgram>("./497LaktionovData/shader.vert",
                                                  "./497LaktionovData/shader.frag");

        _wallTexture = loadTexture("497LaktionovData/wall.JPG");
        _floorTexture = loadTexture("497LaktionovData/floor.JPG");
        _ceilingTexture = loadTexture("497LaktionovData/ceiling.JPG");
        _wallTextureNorm = loadTexture("497LaktionovData/wall_norm.JPG");
        _floorTextureNorm = loadTexture("497LaktionovData/floor_norm.JPG");
        _ceilingTextureNorm = loadTexture("497LaktionovData/ceiling_norm.JPG");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_normSampler);
        glSamplerParameteri(_normSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_normSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_normSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_normSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Устанавливаем шейдер
        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (_cameraMover->spotLight) {
            _shader->setVec3Uniform("light.La", glm::vec3(0.1, 0.1, 0.1));
            _shader->setVec3Uniform("light.Ld", glm::vec3(0.05, 0.05, 0.05));
            _shader->setVec3Uniform("light.Ls", glm::vec3(1.0, 1.0, 1.0));

            _shader->setVec3Uniform("light.pos",
                                    glm::vec3(0.0, 0.0, 5.0)); //копируем положение уже в системе виртуальной камеры
            _shader->setIntUniform("is_light", 1);
        } else {
            _shader->setVec3Uniform("light.La", glm::vec3(0.2, 0.2, 0.2));
            _shader->setVec3Uniform("light.Ld", glm::vec3(0.8, 0.8, 0.8));
            _shader->setVec3Uniform("light.Ls", glm::vec3(1.0, 1.0, 1.0));

            glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(0.0, 0.0, 10.0, 1.0));
            _shader->setVec3Uniform("light.pos",
                                    lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setIntUniform("is_light", 0);
        }

        _shader->setIntUniform("diffuseTex", 0);
        _shader->setIntUniform("normTex", 1);


        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _wallTexture->bind();
        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _normSampler);
        _wallTextureNorm->bind();


        for (auto &mesh : _walls) {
            _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
            mesh->draw();
        }

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _floorTexture->bind();
        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _normSampler);
        _floorTextureNorm->bind();

        for (auto &mesh : _floors) {
            _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
            mesh->draw();
        }

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _ceilingTexture->bind();
        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _normSampler);
        _ceilingTextureNorm->bind();

        for (auto &mesh : _ceiling) {
            _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
            mesh->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main() {
    std::ifstream fin("497LaktionovData/maze.txt");
    int m, l;
    fin >> m >> l;
    std::vector<std::vector<std::vector<char>>> maze(1, std::vector<std::vector<char>>(m, std::vector<char>(l)));
    for (int j = 0; j < m; ++j)
        for (int k = 0; k < l; ++k) {
            int q;
            fin >> q;
            maze[0][j][k] = q;
        }
    MyApplication app(maze);
    app.start();
    return 0;
}
